%{
Matlab applications in physics
Author: Michał Koruszowic
Technical Physics
%}

clear;
addpath('jsonlab-master')

station_name = {'czestochowa', 'katowice', 'raciborz'};

% checking if the file already exist
if exist('weatherData.csv') ~= 2
  % creating a new file
  file_ID = fopen('weatherData.csv','w');
  fclose(file_ID);
  
else
  % list the contents of a folder
  attributes = dir('weatherData.csv');
  
  %checking if the file is empty
  if attributes.bytes ~= 0
    file_ID = fopen('weatherData.csv','r');
    
    % reading data from file
    file_scan = textscan(file_ID,'%s %s %s %f %f %f %f %f', 'Delimiter', ',', 'HeaderLines', 1);

    fclose(file_ID);

    %finding maximum and minimum value in file 
    for k = 1:4
      [max_value(k), max_index(k)] = max(file_scan{k+3});
      [min_value(k), min_index(k)] = min(file_scan{k+3});
    end
    
    % creating varaible for time and date
    date = file_scan{2};
    time = file_scan{3};

    % displaying maximum and minimum values in the command window
    fprintf('Maximum | minimum values registered in weatherData.csv\n');
    
    fprintf('Temperature: %.2f C| %.2f C at %s|%s on %s|%s \n', max_value(1), min_value(1), cell2mat(time(max_index(1))),...
    cell2mat(time(min_index(1))), cell2mat(date(max_index(1))), cell2mat(date(min_index(1)))) ;
    
    fprintf('Sensible temperature: %.2f C| %.2f C at %s|%s on %s|%s \n', max_value(2), min_value(2), cell2mat(time(max_index(2))),...
    cell2mat(time(min_index(2))), cell2mat(date(max_index(2))), cell2mat(date(min_index(2)))) ;
    
    fprintf('Preassure: %.2f hPa| %.2f hPa at %s|%s on %s|%s \n', max_value(3), min_value(3), cell2mat(time(max_index(3))),...
    cell2mat(time(min_index(3))), cell2mat(date(max_index(3))), cell2mat(date(min_index(3)))) ;
    
    fprintf('Humidity: %.2f%%| %.2f%% at %s|%s on %s|%s \n\n', max_value(4), min_value(4), cell2mat(time(max_index(4))),...
    cell2mat(time(min_index(4))), cell2mat(date(max_index(4))), cell2mat(date(min_index(4)))) ;
  end
end

%loop for downloading synoptic data 
while 1
  time = clock;
  % checking current time
  if time(5) == 33   
    attributes = dir('weatherData.csv');
    
    %checking if the file is empty
    if attributes.bytes == 0
      file_ID = fopen('weatherData.csv','a');
      %creating header
      header = {'Station', 'Date', 'Time','Temperature [C]', 'Sensible temperature [C]','Preassure [hPa]',...
      'Humidity [%]', 'Wind velocity [m/s]'};
      fprintf(file_ID, '%s,%s,%s,%s,%s,%s,%s,%s \n', header{:});
      fclose(file_ID);
    end

    %loading synoptic data
    fprintf('Downloading data \n\n');
    
    for k = 1:size(station_name, 2)
        %loading data from wttr and imgw staion
        wttr_stations(k) = loadjson(urlread(cell2mat(['http://wttr.in/' station_name(k) '?format=j1'])));
        imgw_stations(k) = loadjson(urlread(cell2mat(['https://danepubliczne.imgw.pl/api/data/synop/station/' station_name(k) '/format/json'])));
        
        % saving data to variable separataly for imgw and wttr
        temperature_wttr(k) = str2double(wttr_stations(k).current_condition{1}.temp_C); 
        pressure_wttr(k) = str2double(wttr_stations(k).current_condition{1}.pressure);
        wind_velocity_wttr(k) = str2double(wttr_stations(k).current_condition{1}.windspeedKmph)/3.6;
        humidity_wttr(k) = str2double(wttr_stations(k).current_condition{1}.humidity);
        time_wttr{k} = datestr(wttr_stations(k).current_condition{1}.observation_time,15);
        date_wttr{k} = datestr(wttr_stations(k).weather{1}.date,29);
        feels_T_wttr(k) = FeelsLikeT(temperature_wttr(k),wind_velocity_wttr(k),humidity_wttr(k));
        
        temperature_imgw(k) = str2double(imgw_stations(k).temperatura);
        pressure_imgw(k) = str2double(imgw_stations(k).cisnienie);
        wind_velocity_imgw(k) = str2double(imgw_stations(k).predkosc_wiatru);
        humidity_imgw(k) = str2double(imgw_stations(k).wilgotnosc_wzgledna);
        time_imgw{k} = [num2str(imgw_stations(k).godzina_pomiaru),':00'];
        date_imgw{k} = imgw_stations(k).data_pomiaru;
        feels_T_imgw(k) = FeelsLikeT(temperature_imgw(k),wind_velocity_imgw(k),humidity_imgw(k));      
    end
    fprintf('Download completed \n\n');
    
    %writing data to file
    file_ID = fopen('weatherData.csv','a');
    for k = 1:size(station_name, 2)
       fprintf(file_ID, '%s, %s, %s, %f, %f, %f, %f, %f \n', [station_name{k} ' imgw'], date_imgw{k}, time_imgw{k},...
       temperature_imgw(k), feels_T_imgw(k), pressure_imgw(k), humidity_imgw(k), wind_velocity_imgw(k));
       
       fprintf(file_ID, '%s, %s, %s, %f, %f, %f, %f, %f \n', [station_name{k} ' wttr'], date_wttr{k}, time_wttr{k},...
       temperature_wttr(k), feels_T_wttr(k), pressure_wttr(k), humidity_wttr(k), wind_velocity_wttr(k));
    end
    fclose(file_ID);
    
    % reading data from file
    file_ID = fopen('weatherData.csv','r');
    file_scan = textscan(file_ID,'%s %s %s %f %f %f %f %f', 'delimiter', ',', 'HeaderLines', 1);
    fclose(file_ID);

    %finding maximum and minimum value 
    for k = 1:4
      [max_value(k), max_index(k)] = max(file_scan{k+3});
      [min_value(k), min_index(k)] = min(file_scan{k+3});
    end
    
    % creating varaible for time and date
    date = file_scan{2};
    time = file_scan{3};

    % displaying maximum and minimum values in the command window
    fprintf('Maximum | minimum values registered in weatherData.csv\n');
    
    fprintf('Temperature: %.2f C| %.2f C at %s|%s on %s|%s \n', max_value(1), min_value(1), cell2mat(time(max_index(1))),...
    cell2mat(time(min_index(1))), cell2mat(date(max_index(1))), cell2mat(date(min_index(1)))) ;
    
    fprintf('Sensible temperature: %.2f C| %.2f C at %s|%s on %s|%s \n', max_value(2), min_value(2), cell2mat(time(max_index(2))),...
    cell2mat(time(min_index(2))), cell2mat(date(max_index(2))), cell2mat(date(min_index(2)))) ;
    
    fprintf('Preassure: %.2f hPa| %.2f hPa at %s|%s on %s|%s \n', max_value(3), min_value(3), cell2mat(time(max_index(3))),...
    cell2mat(time(min_index(3))), cell2mat(date(max_index(3))), cell2mat(date(min_index(3)))) ;
    
    fprintf('Humidity: %.2f%%| %.2f%% at %s|%s on %s|%s \n\n', max_value(4), min_value(4), cell2mat(time(max_index(4))),...
    cell2mat(time(min_index(4))), cell2mat(date(max_index(4))), cell2mat(date(min_index(4)))) ;
    
    % wating 1 min
    pause(60)
  end 
end

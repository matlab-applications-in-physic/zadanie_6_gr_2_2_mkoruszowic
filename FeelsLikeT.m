%Code from jfcarr
%Link: https://gist.github.com/jfcarr/e68593c92c878257550d
function value = FeelsLikeT(temperature, wind_velocity, humidity)
  
  temperature_F = temperature* 1.8 + 32;
  wind_velocity_mph = wind_velocity * 2.2369;
  
  % Try Wind Chill first
  if temperature_F <= 50 && wind_velocity_mph >= 3
    value = 35.74 + (0.6215*temperature_F) - 35.75*(wind_velocity_mph^0.16) + ((0.4275*temperature_F)*(wind_velocity_mph^0.16));
  else
    value = temperature_F;
  end
  
  % Replace it with the Heat Index, if necessary
  if value == temperature_F && temperature_F >= 80
    value = 0.5 * (temperature_F + 61.0 + ((temperature_F-68.0)*1.2) + (humidity*0.094));
  end
  
  if value >= 80
    value = -42.379 + 2.04901523*temperature_F + 10.14333127*humidity - .22475541*temperature_F*humidity - .00683783*temperature_F*temperature_F - .05481717*humidity*humidity + .00122874*temperature_F*temperature_F*humidity + .00085282*temperature_F*humidity*humidity - .00000199*temperature_F*temperature_F*humidity*humidity;
    if humidity < 13 && temperature_F >= 80 && temperature_F <= 112
      value = value - ((13-humidity)/4)*math.sqrt((17-abs(temperature_F-95.))/17);
      if humidity > 85 && temperature_F >= 80 && temperature_F <= 87
        value = value + ((humidity-85)/10) * ((87-temperature_F)/5);
      end
    end
  end
  value = (value -32)/1.8;
end 